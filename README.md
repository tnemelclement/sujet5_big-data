*Nada SRAIGAIN - Clément MOULY*

## Prérequis
- `python3`
- `java 11`
- `docker` et `docker-compose`

## Installation (en mode developpement)
1. Cloner le repository git : `https://gitlab.com/tnemelclement/sujet5_big-data.git`
2. Installation de la base de donnée :
	1. Allez dans le dossier `database`
	2. Exécutez la commande `docker-compose up -d` (assurez vous ne rien avoir sur le port `55432`)
3. Installation de Kafka :
	1. Aller dans le dossier `server-kafka`
	2. Exécutez la commande `docker-compose up -d`
4. Installez ensuite les dépendances maven de `producer` et `analyse`
5. Installez les librairie python liées aux scripts.

![Figure 1](image.png "Figure 1"){width=75%}

## Execution

Une fois que les jars sont générés et les conteneurs démarrés pour `kafka` et `postgres`.
- **Lancez** le jar `consumer.jar` avec la commande `java -jar consumer.jar`
- *Des logs s'affichent et le consumer attend de récupérer des éléments de Kafka.*
- **Lancez** le script python `kafka_producer.py` pour envoyer les données vers Kafka.
- *Le consumer affiche les données au fur et à mesure qu'elles arrivent, les filtre et stocke les données valides dans la base de données.*
- **Lancez** le jar `analyse.jar` avec la commande `java -jar analyse.jar <type-etablissement>`
- *Les données sont récupérées depuis la base de données et une requête est appliquée pour afficher le nombre de `<type-etablissement>` par départements.*
- *Un fichier CSV est généré avec les données.*
- Il faut ensuite **importer** ce CSV dans Gephi pour visualiser les données.
