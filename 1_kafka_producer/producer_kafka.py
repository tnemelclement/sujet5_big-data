from confluent_kafka import Producer
import csv
import json


csv_file_path = 'ecoles.csv'  


# Configuration du producteur Kafka
conf = {
    'bootstrap.servers': 'localhost:19092',
    'client.id': 'python-producer'
}

# Création de l'objet producteur
producer = Producer(conf)

try:
    print("Starting the script...")

    with open(csv_file_path, mode='r', newline='') as csv_file:
        print("CSV file opened successfully...")
        csv_reader = csv.DictReader(csv_file, delimiter=';')

        for row in csv_reader:
            json_data = json.dumps(row)
            print(f"Producing message: {json_data}")
            producer.produce('ecoles', key=None, value=json_data)

    producer.flush()
    print("Script execution completed.")

except KeyboardInterrupt:
    print("\nScript execution stopped by the user.")

except Exception as e:
    print(f"An error occurred: {e}")
