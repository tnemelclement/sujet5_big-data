package episen;

import org.apache.spark.SparkConf;
import org.apache.spark.sql.*;
import org.apache.spark.sql.streaming.StreamingQuery;
import org.apache.spark.sql.streaming.StreamingQueryException;
import org.apache.spark.sql.types.StructType;
import java.util.concurrent.TimeoutException;

public class App {
    public static void main(String[] args) {
        try {
            SparkConf conf = new SparkConf();
            conf.setAppName("consumer");
            conf.setMaster("local[*]");

            SparkSession session = SparkSession.builder()
                    .config(conf)
                    .getOrCreate();

            session.sparkContext().setLogLevel("WARN");
            session.sparkContext().log().warn("consumer");

            Dataset<Row> kafkaData = session
                    .readStream()
                    .format("kafka")
                    .option("kafka.bootstrap.servers", "localhost:19092")
                    .option("subscribe", "ecoles")
                    .load()
                    .selectExpr("CAST(value AS STRING)");

            StructType schema = new StructType()
                    .add("numero_uai", "string")
                    .add("appellation_officielle", "string")
                    .add("denomination_principale", "string")
                    .add("code_commune", "string")
                    .add("code_departement", "string")
                    .add("nature_uai", "string"); // Adjust the type as necessary

            Dataset<Row> df = kafkaData.select(
                            functions.from_json(kafkaData.col("value"), schema).as("data")
                    )
                    .select("data.*");


            Dataset<Row> premierDegre = df.filter(
                    functions.col("nature_uai").cast("int").between(100, 199)
            );

            Dataset<Row> secondDegre = df.filter(
                    functions.col("nature_uai").cast("int").between(300, 399)
            );

            Dataset<Row> filteredData = premierDegre.union(secondDegre);

            System.out.println("df"+df);
            kafkaData.createOrReplaceTempView("schools");


            String postgresURL = "jdbc:postgresql://localhost:55432/ecoles";
            String tableName = "ecoles";
            String username = "user";
            String password = "password";

            StreamingQuery query = filteredData.writeStream()
                    .outputMode("append")
                    .foreachBatch((batchDF, batchId) -> {
                        batchDF.show(); // Add this line to display the contents of the DataFrame
                        // Uncomment the database write logic once DataFrame content is confirmed
                        batchDF.write()
                                .format("jdbc")
                                .option("url", postgresURL)
                                .option("dbtable", tableName)
                                .option("user", username)
                                .option("password", password)
                                .mode("append")
                                .save();
                    })
                    .start();
            query.awaitTermination();

        } catch (StreamingQueryException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            throw new RuntimeException(e);
        }
    }
}
