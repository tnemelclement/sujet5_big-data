package episen;

import org.apache.spark.SparkConf;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import static org.apache.spark.sql.functions.col;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        try {
            String denominationPrincipale = args[0] != null ? args[0] : "COLLEGE";

            SparkConf conf = new SparkConf();
            conf.setAppName("dataanalyse");
            conf.setMaster("local[*]");
            SparkSession session = SparkSession.builder().config(conf).getOrCreate();

            /* *** Base de données *** **/
            String url = "jdbc:postgresql://localhost:55432/ecoles";
            String user = "user";
            String password = "password";
            Dataset<Row> jdbcDF = session.read()
                    .format("jdbc")
                    .option("url", url)
                    .option("user", user)
                    .option("password", password)
                    .option("dbtable", "ecoles")
                    .load();


            // Créer une DataFrame avec le paramètre
            Dataset<Row> result = jdbcDF
                    .filter(col("denomination_principale").equalTo(denominationPrincipale))
                    .groupBy(col("code_departement"))
                    .count()
                    .withColumnRenamed("count", "nombre_ecoles");

            String outputPath = "result_analyse.csv"; // Set your desired output path
            result.coalesce(1).write().format("csv").option("header", "true").save(outputPath);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
